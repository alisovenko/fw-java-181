package lec3_oop.exercises;

/**
 * Organization is the object containing fields for name, date (day, month, year) and number of employees
 * Your task is to create the class and create all the methods that listed in this class.
 * Don't create any other methods! It will be considered as an error!
 */
public class OrganizationTest {
    public static void main(String[] args) {
        final Organization org = new Organization("BP", 2, 8, 2017);
        // Here we check that constructor did correct work of initializing all the fields
        if (!org.getName().equals("BP")) {
            System.out.println("Wrong name!");
        }
        if (org.getDayCreated() != 2) {
            System.out.println("Wrong day of creation! " + org.getDayCreated());
        }
        if (org.getMonthCreated() != 8) {
            System.out.println("Wrong month of creation! " + org.getMonthCreated());
        }
        if (org.getYearCreated() != 2017) {
            System.out.println("Wrong year of creation! " + org.getYearCreated());
        }
        // This method is supposed to return the date as a string. Check String.format(..) for the ways to create a formatted string
        final String dateCreated = org.getDateCreated();
        if (!dateCreated.equals("2.8.2017")) {
            System.out.println("Wrong output for date! " + dateCreated);
        }
        org.hireEmployees(100);
        if (org.getNumberOfEmployees() != 100) {
            System.out.println("Wrong number of employees! " + org.getNumberOfEmployees());
        }
        // when we fire more employees than we have - we should not proceed the operation!
        // Your method must output the message "Not possible to fire more than employed!" in this case
        org.fireEmployees(200);
        if (org.getNumberOfEmployees() != 100) {
            System.out.println("Wrong number of employees! " + org.getNumberOfEmployees());
        }
        // This operation must succeed
        org.fireEmployees(40);
        if (org.getNumberOfEmployees() != 60) {
            System.out.println("Wrong number of employees! " + org.getNumberOfEmployees());
        }
    }
}