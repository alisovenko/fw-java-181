package lec3_oop.exercises;

public class Organization {
    private String name;

    private int dayCreated;

    private int monthCreated;

    private int yearCreated;

    private int numberOfEmployees;

    public Organization(String name, int dayCreated, int monthCreated, int yearCreated) {
        this.name = name;
        this.dayCreated = dayCreated;
        this.monthCreated = monthCreated;
        this.yearCreated = yearCreated;
    }

    public String getName() {
        return name;
    }

    public int getDayCreated() {
        return dayCreated;
    }

    public int getMonthCreated() {
        return monthCreated;
    }

    public int getYearCreated() {
        return yearCreated;
    }

    public String getDateCreated() {
        return String.format("%d.%d.%d", dayCreated, monthCreated, yearCreated);
    }

    public int getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void hireEmployees(int employeesNumber) {
        numberOfEmployees = numberOfEmployees + employeesNumber;
    }

    public void fireEmployees(int number) {
        if (number > numberOfEmployees) {
            System.out.println("Not possible to fire more than employed!");
            return;
        }
        numberOfEmployees = numberOfEmployees - number;
    }
}
