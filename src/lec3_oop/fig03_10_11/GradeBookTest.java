package lec3_oop.fig03_10_11;// Fig. 3.11: GradeBookTest.java
// GradeBook constructor used to specify the course name at the 
// time each GradeBook object is created.


public class GradeBookTest {

    // main method begins program execution
    public static void main(String args[]) {
        // create GradeBook object
        GradeBook gradeBook1 = new GradeBook(
            "CS101 Introduction to Java Programming", "foo");
        GradeBook gradeBook2 = new GradeBook(
            "CS102 Data Structures in Java", "foo");

        // display initial value of courseName for each GradeBook
        System.out.printf("gradeBook1 course name is: %s\n",
            gradeBook1.getCourseName());
        System.out.printf("gradeBook2 course name is: %s\n",
            gradeBook2.getCourseName());
    } // end main

} // end class GradeBookTest

class Some {
    private int foo;

    public Some(int foo) {
        this.foo = foo;
    }

    public int getFoo() {
        return foo;
    }

    public void setFoo(int foo) {
        this.foo = foo;
    }
}


