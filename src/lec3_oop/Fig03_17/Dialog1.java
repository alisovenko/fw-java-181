package lec3_oop.Fig03_17;// Fig. 3.17: Dialog1.java
// Printing multiple lines in dialog box.

import javax.swing.JOptionPane;

public class Dialog1 {

    public static void main(String args[]) {
        // display a dialog with a message
        JOptionPane.showMessageDialog(null, "Welcome\nto\nJava, dude!");
    } // end main
} // end class Dialog1

