package lec17_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PreparedStatementPractice {
    static final String DATABASE_URL = "jdbc:mysql://localhost/books";

    public static void main(String[] args) throws SQLException {
        try (Connection conn = DriverManager.getConnection(DATABASE_URL, "jhtp7", "jhtp7");
             PreparedStatement sql = conn.prepareStatement("SELECT lastName, firstName, title " +
                     "FROM authors INNER JOIN authorISBN " +
                     "ON authors.authorID=authorISBN.authorID " +
                     "INNER JOIN titles " +
                     "ON authorISBN.isbn=titles.isbn " +
                     "WHERE firstName = ? AND lastName = ?");) {
            printTable(sql, "Harvey", "Deitel");
            printTable(sql, "Andrew", "Goldberg");
        }
    }

    private static void printTable(PreparedStatement sql, String firstName, String lastName)
            throws SQLException {
        sql.setString(1, firstName);
        sql.setString(2, lastName);

        System.out.println();
        System.out.printf("All books for the query '%s', '%s'\n", firstName, lastName);
        try (ResultSet rs = sql.executeQuery()) {
            while (rs.next())
                System.out.printf("%-10s%-10s%-20s\n", rs.getString(1), rs.getString(2), rs.getString(3));
        }
    }
}
