package lec15_collections.linkedlist;

import java.util.LinkedList;

public class LinkedListPractice {
    private static final String colors[] = {"black", "yellow",
            "green", "blue", "violet", "silver"};

    public static void main(String[] args) {
        final LinkedList<String> colorsList = new LinkedList<>();

        for (String colour : colors) {
            colorsList.add(colour);
        }

        // These are methods from List interface

        System.out.println(colorsList.get(2));
        colorsList.set(5, "grey");
        colorsList.remove(3);

        // These are methods from Deque interface

        System.out.println(colorsList.pollFirst()); // retrieves (and removes) the head element: black
        System.out.println(colorsList.peekFirst()); // retrieves (but doesn't remove) the head: yellow

        colorsList.offer("orange"); // adds the element as the last one
        System.out.println(colorsList.pollLast()); // retrieves (and removes) the tail element: orange
        System.out.println(colorsList.peekLast()); // retrieves (but doesn't remove) the tail: grey
    }
}
