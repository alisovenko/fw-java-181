package lec2_basics.exercise;

import java.util.Scanner;

public class Product {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the first number");

        int num1 = scanner.nextInt();
        System.out.println("Enter the second number");

        int num2 = scanner.nextInt();

        System.out.println("Enter the third number");

        int num3 = scanner.nextInt();

        System.out.printf("Product: %d * %d * %d = %d\n", num1, num2, num3, num1 * num2 * num3);
    }
}
