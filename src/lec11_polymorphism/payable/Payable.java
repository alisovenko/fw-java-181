package lec11_polymorphism.payable;
// Payable interface declaration.

public interface Payable {
    double getPaymentAmount(); // calculate payment; no implementation
}
