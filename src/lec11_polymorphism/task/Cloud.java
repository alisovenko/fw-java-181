package lec11_polymorphism.task;

import java.util.List;

/**
 * Cloud interface represents a general cloud implementation that allows user to add files locally
 * and "synchronize" them with remote cloud storage.
 */
public interface Cloud {
    /**
     * Adds a file to the local directory tracked by cloud provider (as the example - copying file
     * locally to the directory managed by Dropbox for example)
     *
     * @param file the name of file
     * @return if the file was added successfully
     */
    boolean addFileLocally(String file);

    /**
     * @return the list of files that were synchronized with remote cloud storage.
     */
    List<String> getAllSyncedFiles();

    /**
     * Performs synchronization of "local" files with remote cloud storage. As the result all local files will disappear
     * and will move to the remote storage (this is a bit contrived example just to show some programming practices)
     *
     * @return if synchronization ended successfully
     */
    boolean sync();

    /**
     * Updates the current settings for the cloud
     */
    void updateSettings(String properties);

    /**
     * @return current settings for the cloud
     */
    String readSettings();
}
