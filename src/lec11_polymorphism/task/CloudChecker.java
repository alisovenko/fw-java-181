package lec11_polymorphism.task;

public class CloudChecker {
    public static void check(Cloud cloud) {
        System.out.println("\n*** Test for cloud provider: " + cloud.getClass().getSimpleName());

        // Adding one file
        System.out.println("Adding file \"test\"");
        cloud.addFileLocally("test");
        System.out.println(cloud.getAllSyncedFiles());

        System.out.println("Syncing...");
        cloud.sync();
        System.out.println(cloud.getAllSyncedFiles());

        // TODO add the second file ("test 2") to the cloud, call sync() method and output all the files in cloud after this
        // ......

        cloud.updateSettings("user: Bob, password: qwerty, country: Spain");
        System.out.println(cloud.readSettings());
    }
}
