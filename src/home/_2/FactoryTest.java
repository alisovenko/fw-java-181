package home._2;

/**
 * Factory is the class that stores information about name, address, number of employees and average productivity of
 * employee (number of goods produced in average by employee).
 * Create the class, add all necessary fields, constructors and methods, make sure this class compiles.
 * Run the class and make sure it contains no output starting with "Wrong..."
 */
public class FactoryTest {
    public static void main(String[] args) {
        Factory f = new Factory("P&G", "Green Rd, Crotanstown, Newbridge, Co. Kildare");

        // "equals() is the method of class String that is used to compare String objects for equality
        if (!f.getName().equals("P&G")) {
            System.out.println("Wrong name! " + f.getName());
        }

        if (!f.getAddress().equals("Green Rd, Crotanstown, Newbridge, Co. Kildare")) {
            System.out.println("Wrong address! " + f.getAddress());
        }

        f.setEmployees(50);

        // employees number cannot be negative!
        f.setEmployees(-4);

        if (f.getEmployees() != 50) {
            System.out.println("Wrong number of employees! " + f.getEmployees());
        }

        f.setAvgEmployeeProductivity(3);

        // productivity cannot be negative!
        f.setAvgEmployeeProductivity(-10);

        if (f.getAvgEmployeeProductivity() != 3) {
            System.out.println("Wrong avg employee productivity! " + f.getAvgEmployeeProductivity());
        }

        // display() method shows the information about object in human-readable format
        if (!f.display().equals("Factory \"P&G\" has 50 employees and is located by address \"Green Rd, Crotanstown, Newbridge, Co. Kildare\"")) {
            System.out.println("Wrong display message! " + f.display());
        }

        // goodsProduced() method calculates number of goods that factory produces in average during specified number
        // of days. Use objects fields to calculate it. Don't create a separate field for this result - just
        // return from the method.
        if (f.goodsProduced(5) != 750) {
            System.out.println("Wrong number of goods produced during 5 days! " + f.goodsProduced(5));
        }

        // number of days cannot be negative - just return 0 in this case.
        if (f.goodsProduced(-2) != 0) {
            System.out.println("Wrong number of days - it cannot be negative so 0 goods can be produced! " + f.goodsProduced(5));
        }
    }
}
