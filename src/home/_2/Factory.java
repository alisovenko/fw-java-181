package home._2;

public class Factory {
    private int employees;
    private int avgEmployeeProductivity;
    private String address;
    private String name;

    public Factory(String name, String address) {
        this.address = address;
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public int getEmployees() {
        return employees;
    }

    public void setEmployees(int employees) {
        if (employees < 0) {
            System.out.println("Employees number cannot be negative!");
            return;
        }
        this.employees = employees;
    }

    public int getAvgEmployeeProductivity() {
        return avgEmployeeProductivity;
    }

    public void setAvgEmployeeProductivity(int avgEmployeeProductivity) {
        if (avgEmployeeProductivity < 0) {
            System.out.println("Goods parameter cannot be negative!");
            return;
        }
        this.avgEmployeeProductivity = avgEmployeeProductivity;
    }

    public String display() {
        return String.format("Factory \"%s\" has %d employees and is located by address \"%s\"", name, employees, address);
    }

    public int goodsProduced(int days) {
        if (days < 0) {
            System.out.println("Days parameter cannot be negative!");
            return 0;
        }
        return employees * avgEmployeeProductivity * days;
    }
}
