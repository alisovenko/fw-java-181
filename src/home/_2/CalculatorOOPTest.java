package home._2;

public class CalculatorOOPTest {
    public static void main(String[] args) {
        final CalculatorOOP calculator = new CalculatorOOP(Integer.MAX_VALUE, Integer.MAX_VALUE);

        System.out.println(calculator.sum());
        System.out.println(calculator.difference());
        System.out.println(calculator.multiplication());
        System.out.println(calculator.division());

        calculator.printReport();
    }
}
