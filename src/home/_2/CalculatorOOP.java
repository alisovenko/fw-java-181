package home._2;

public class CalculatorOOP {
    private int firstNum;

    private int secondNum;

    public CalculatorOOP(int firstNum, int secondNum) {
        this.firstNum = firstNum;
        this.secondNum = secondNum;
    }

    public int getFirstNum() {
        return firstNum;
    }

    public void setFirstNum(int firstNum) {
        this.firstNum = firstNum;
    }

    public int getSecondNum() {
        return secondNum;
    }

    public void setSecondNum(int secondNum) {
        this.secondNum = secondNum;
    }

    public long sum() {
        return (long) firstNum + secondNum;
    }

    public int difference() {
        return firstNum - secondNum;
    }

    public long multiplication() {
        return (long) firstNum * secondNum;
    }

    public void printReport() {
        System.out.println("#########################################");
        System.out.printf("Calculation results for numbers %d and %d\n", firstNum, secondNum);
        System.out.println("#########################################");
        System.out.printf("%-15s:%d\n%-15s:%d\n%-15s:%d\n%-15s:%.2f\n", "Sum", sum(), "Difference", difference(),
                "Multiplication", multiplication(), "Division", division());
    }

    public float division() {
        if (secondNum == 0) {
            System.out.println("Cannot perform division to 0!");
            return 0;
        }
        return (float) firstNum / secondNum;
    }
}
