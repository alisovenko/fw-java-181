package home._2;

import java.util.Date;

public class Directory {
    private String name;
    // TODO this is a simplification of real use case: in reality these would be the references to the array of type File[]
    private String[] files;
    private String[] parentPath;
    private boolean system;
    private boolean hidden;
    private Date created;
    private Date updated;
    private String[] comments;
    private String[] usersWithReadPermissions;
    private String[] usersWithWritePermissions;

    // In real operational system the only mandatory things when we create a directory are its name and its path!
    public Directory(String name, String[] parentPath) {
        this.name = name;
        this.parentPath = parentPath;

        // These fields are supported by the system, user shouldn't be able to change them!
        created = new Date();
        updated = created;
    }

    public String getName() {
        return name;
    }

    public String[] getFiles() {
        return files;
    }

    public String[] getParentPath() {
        return parentPath;
    }

    public boolean isSystem() {
        return system;
    }

    public boolean isHidden() {
        return hidden;
    }

    public Date getCreated() {
        return created;
    }

    public Date getUpdated() {
        return updated;
    }

    public String[] getComments() {
        return comments;
    }

    public String[] getUsersWithReadPermissions() {
        return usersWithReadPermissions;
    }

    public String[] getUsersWithWritePermissions() {
        return usersWithWritePermissions;
    }

    // This is equal to renaming operation for the directory
    public void setName(String name) {
        if (name == null) {
            // We cannot make a name empty!
            return;
        }
        updated = new Date();
        this.name = name;
    }

    // This operation is like "moving" the directory to another one
    public void setParentPath(String[] parentPath) {
        if (parentPath == null) {
            // We cannot put a directory to an empty path!
            return;
        }
        updated = new Date();
        this.parentPath = parentPath;
    }

    public void setSystem(boolean system) {
        if (system != this.system) {
            updated = new Date();
        }
        this.system = system;
    }

    public void setHidden(boolean hidden) {
        if (hidden != this.hidden) {
            updated = new Date();
        }
        this.hidden = hidden;
    }

    public void addFile(String fileName) {
        // TODO check that a file with such a name is already there
        // TODO Add a file to files array
    }

    public boolean removeFile(String fileName) {
        // TODO return false if the file is not in the directory
        // TODO delete a file from files array
        return true;
    }

    public boolean fileExists(String fileName) {
        // TODO check if the file with such name exists in the directory and return true/false
        return true;
    }

    public void addUserReadPermission(String userName, boolean canRead ) {
        // TODO add user to usersWithReadPermissions array or remove if canRead is false
    }

    public void addUserWritePermission(String userName, boolean canWrite) {
        // If user can write to directory - then he is supposed to be able to read as well
        if (canWrite)
            addUserReadPermission(userName, true);
        // TODO add user to usersWithWritePermissions array or remove if canWrite is false
    }

    public void addComment(String comment) {
        updated = new Date();
        // TODO add a comment to comments array
    }

    public int getSize() {
        // TODO this is a calculated number: iterate through files and accumulate their sizes - return this value
        return 0;
    }
}
