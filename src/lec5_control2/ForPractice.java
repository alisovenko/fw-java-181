package lec5_control2;

public class ForPractice {
    public static void main(String[] args) {
        for (int start = 0, end = 20; start < end; start++, end--) {
            System.out.printf("start: %d, end: %d\n", start, end);
        }

    }
}
