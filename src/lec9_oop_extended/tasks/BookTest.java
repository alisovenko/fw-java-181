package lec9_oop_extended.tasks;

public class BookTest {
  public static void main(String[] args) {
    Author bill = new Author("Bill", "a@gmail.com", 'm');
    Author james = new Author("James", "j@gmail.com", 'm');
    Author louise = new Author("Louise", "l@gmail.com", 'w');

    Author[] pair2 = {louise, james};
    Author[] pair3 = {louise};

    Book javaBook = new Book("java", 40, new Author[]{bill, james});
    Book cPlusBook = new Book("c++", 50, pair2);
    Book pythonBook = new Book("python", 50, pair3);

    System.out.println(javaBook);
  }
}
