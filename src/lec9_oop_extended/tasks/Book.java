package lec9_oop_extended.tasks;

import java.util.Arrays;

public class Book {
  private String name;
  private double price;
  private Author[] authors;
  private int qty;

  public Book(String name, double price, Author[] authors) {
    this(name, price, authors, 0);
  }

  public Book(String name, double price, Author[] authors, int qty) {
    this.name = name;
    this.price = price;
    this.authors = authors;
    this.qty = qty;
  }

  public String getName() {
    return name;
  }

  public Author[] getAuthors() {
    return authors;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public int getQty() {
    return qty;
  }

  public void setQty(int qty) {
    this.qty = qty;
  }

  @Override
  public String toString() {
    for (Author a : authors) {
      System.out.println(a);
    }
    return String.format("Book: name = %s, authors = %s", name, Arrays.toString(authors));
  }
}
