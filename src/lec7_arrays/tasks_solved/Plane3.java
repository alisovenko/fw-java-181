package lec7_arrays.tasks_solved;

import java.util.Scanner;

/**
 * This version adds the functionality for printing the schema of seats
 *
 * @author alisovenko
 *         4/19/17.
 */
public class Plane3 {
    public static void main(String[] args) {
        boolean[] seats = new boolean[12];
        int firstClassCounter = 0;
        int economyCounter = 6;
        Scanner scanner = new Scanner(System.in);

        while (firstClassCounter < 6 || economyCounter < 12) {
            System.out.println();
            System.out.println("Please type 1 for First Class, 2 for Economy and 3 for printing seat map");
            int c = scanner.nextInt();

            switch (c) {
                case 1:
                    firstClassCounter = registerSeat(seats, firstClassCounter, "first class", 6);
                    break;
                case 2:
                    economyCounter = registerSeat(seats, economyCounter, "economy", 12);
                    break;
                case 3:
                    printSeats(seats);
                    break;
                default:
                    System.err.println("Please enter correct number!");
            }
        }
        System.out.println("\nNo seats left! Next flight leaves in 3 hours");
    }

    private static int registerSeat(boolean[] seats, int counter, String className, int threashold) {
        if (counter >= threashold) {
            System.out.printf("Unfortunately all seats for class %s are booked, you can try other class!\n", className);
            return counter;
        }

        seats[counter++] = true;
        System.out.printf("Congratulations, you've just booked seat number %d in class %s!\n", counter, className);
        return counter;
    }

    private static void printSeats(boolean[] seats) {
        for (int i = 0; i < seats.length; i++) {
            if (i % 2 == 0)
                System.out.println();

            if (i == 0)
                System.out.println("First-class\n");
            else if (i == 6)
                System.out.println("Economy\n");

            if (seats[i])
                System.out.print('*');
            else
                System.out.print('_');
        }
    }
}
