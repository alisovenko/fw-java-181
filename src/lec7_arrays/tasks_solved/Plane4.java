package lec7_arrays.tasks_solved;

import java.util.Scanner;

/**
 * This is a pure OOP version of Plane system which encapsulates the plane system data inside and exposes only two methods
 * to client: bookSeat and printSeats.
 *
 * @author alisovenko
 *         4/19/17.
 */
public class Plane4 {
    private static final int FIRST_CLASS_THRESHOLD = 6;
    private static final int ECONOMY_THRESHOLD = 12;
    private boolean[] seats = new boolean[12];
    private int firstClassCounter = 0;
    private int economyCounter = 6;
    private Scanner scanner = new Scanner(System.in);

    /**
     * Tries to book the next available seat for class {@code classChoice}
     *
     * @param classChoice either 1 for first class or 2 for economy
     * @return false if no seats were booked as plane is all occupied, true otherwise
     */
    public boolean bookSeat(int classChoice) {
        if (economyCounter >= ECONOMY_THRESHOLD && firstClassCounter >= FIRST_CLASS_THRESHOLD) {
            System.out.println("All seats are booked!");
            return false;
        }

        if (classChoice == 1)
            firstClassCounter = registerSeat(firstClassCounter, "first class", "economy", FIRST_CLASS_THRESHOLD, 2);
        else if (classChoice == 2)
            economyCounter = registerSeat(economyCounter, "economy", "first class", ECONOMY_THRESHOLD, 1);
        else
            System.out.println("Wrong choice number!"); // better raise the exception here
        return true;
    }

    /**
     * Prints seats map.
     */
    public void printSeats() {
        for (int i = 0; i < seats.length; i++) {
            if (i % 2 == 0)
                System.out.println();

            if (i == 0)
                System.out.println("First-class");
            else if (i == 6)
                System.out.println("\nEconomy");

            if (seats[i])
                System.out.print('*');
            else
                System.out.print('_');
        }
    }

    private int registerSeat(int counter, String className, String otherClassName, int threshold, int otherClass) {
        if (counter >= threshold) {
            System.out.printf("Unfortunately all %s seats are booked, do you want to try %s instead? Print 1 if yes and any other number if not\n", className, otherClassName);

            if (scanner.nextInt() == 1)
                bookSeat(otherClass);

            return counter;
        }

        seats[counter++] = true;
        System.out.printf("Congratulations, you've just booked the %s seat number %d!\n", className, counter);
        return counter;
    }
}
