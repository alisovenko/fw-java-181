package lec6_methods.ex._3;

public class Car {
    private final int gridSize;
    private int x;
    private int y;

    public Car(int x, int y, int gridSize) {
        this.gridSize = gridSize;
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void moveRight() {
        if (y == gridSize - 1) {
            System.out.println("Cannot move right: reached the right border!");
            return;
        }
        y++;
    }
    public void moveLeft() {
        if (y == 0) {
            System.out.println("Cannot move left: reached the left border!");
            return;
        }
        y--;
    }
    public void moveUp() {
        if (x == 0) {
            System.out.println("Cannot move up: reached the upper border!");
            return;
        }
        x--;
    }
    public void moveDown() {
        if (x == gridSize - 1) {
            System.out.println("Cannot move down: reached the lower border!");
            return;
        }
        x++;
    }

    public String currentLocation() {
        return String.format("Car is at [%d, %d]", x, y);
    }
}
