package lec6_methods.ex._3;

import java.util.Scanner;

public class CarGame {
    public static void main(String[] args) {
        char nextAction = 'c';
        Scanner scanner = new Scanner(System.in);
        int gridSize = 5;
        Car car = new Car(0, 0, gridSize);

        System.out.printf("Welcome to the car game! Your car is starting from [%d, %d], the size of board is %dx%d\n", car.getX(), car.getY(), gridSize, gridSize);
        while (true) {
            System.out.println("\nMove your car ('l', 'r', 'u' or 'd') or cancel the game ('c'):");
            String action = scanner.next();
            nextAction = action.charAt(0);

            switch (nextAction) {
                case 'l':
                    car.moveLeft();
                    break;
                case 'r':
                    car.moveRight();
                    break;
                case 'u':
                    car.moveUp();
                    break;
                case 'd':
                    car.moveDown();
                    break;
                case 'c':
                    return;
                default:
                    System.out.printf("Unknown symbol %s!\n", nextAction);
            }

            System.out.println(car.currentLocation());
        }
    }
}
